Repo con snippets de vs code para lwc

Code snippets are templates that make it easier to enter repeating code patterns, such as loops or conditional-statements.

Instrucciones para importarlo (yo los creé como Global Snippets) en
https://code.visualstudio.com/docs/editor/userdefinedsnippets

You can define your own snippets, either global snippets or snippets for a specific language. To open up a snippet file for editing, select User Snippets under File > Preferences (Code > Preferences on macOS) and select the language (by language identifier) for which the snippets should appear or create a new global snippet (New Global Snippets file).

Global snippets that are applicable whenever you are editing and are stored in <name>.code-snippets files, for example MyGlobal.code-snippets. The JSON schema of global snippets allows you to define a scope property which can filter the languages (based on language identifier) for which a snippet is applicable.